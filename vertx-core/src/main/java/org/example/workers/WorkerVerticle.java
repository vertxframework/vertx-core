package org.example.workers;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;

public class WorkerVerticle extends AbstractVerticle {
    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        // will be called in event-loop thread
       vertx.executeBlocking(event -> {
           System.out.println("First Block : ");
           try {
               Thread.sleep(10000);
           } catch (InterruptedException e) {
               throw new RuntimeException(e);
           }
           System.out.println(Thread.currentThread().getName());
       });
       // will be called in event-loop-thread
        vertx.executeBlocking(event -> {
            System.out.println("Second Block : ");
            System.out.println(Thread.currentThread().getName());
        });

        // this will called in event loop
        System.out.println("Last Call : " + Thread.currentThread().getName());
    }

    public static void main(String[] args) {

        Vertx.vertx().deployVerticle(new WorkerVerticle());
    }
}
