package org.example.workers;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;

public class DeployWorker extends AbstractVerticle {
    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        // here this message will print in the event-loop worker thread
        System.out.println(Thread.currentThread().getName());
    }

    public static void main(String[] args) {

        // set the worker with deployment
        Vertx.vertx().deployVerticle(new DeployWorker(),
                new DeploymentOptions().setWorker(true)
                );

    }
}
