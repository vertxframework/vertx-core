package org.example.eventloop;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

import java.util.concurrent.TimeUnit;

public class MainVerticle extends AbstractVerticle {
    public static void main(String[] args) {
        var vertx = Vertx.vertx(new VertxOptions()
                .setMaxEventLoopExecuteTime(2000)
                .setMaxEventLoopExecuteTimeUnit(TimeUnit.MILLISECONDS)

        );


        // blocking operation should be added in worker thread not in event-loop thread

        vertx.deployVerticle(new MainVerticle());
    }
    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        System.out.println(Thread.currentThread().getName() + "   ");
        Thread.sleep(2000);
    }
}
