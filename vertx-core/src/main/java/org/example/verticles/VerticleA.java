package org.example.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;

public class VerticleA extends AbstractVerticle {

    @Override
    public void start(Promise<Void> startPromise) throws Exception {

        System.out.println(VerticleA.class.getName() + "  " + Thread.currentThread().getName());
    }

    @Override
    public void stop(Promise<Void> stopPromise) throws Exception {
        System.out.println(config().encode());
    }
}