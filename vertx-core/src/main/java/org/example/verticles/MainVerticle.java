package org.example.verticles;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

public class MainVerticle {

    public static void main(String[] args) {
        // deploy more than one instance
        // each Verticle work in different event-loop thread but with
        // large number of instances will split the threads between them
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(VerticleA.class.getName(),new DeploymentOptions()
                .setInstances(20)
                .setConfig(new JsonObject().put("name","main Verticle"))

                );

    }
}
